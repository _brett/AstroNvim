-- Mapping data with "desc" stored directly by vim.keymap.set().
--
-- Please use this mappings table to set keyboard mapping since this is the
-- lower level configuration and more robust one. (which-key will
-- automatically pick-up stored data by this setting.)

local utils = require "astronvim.utils"

return {
  -- first key is the mode
  n = {
    -- second key is the lefthand side of the map
    -- mappings seen under group name "Buffer"
    ["<F8>"] = { "<cmd>Flote<cr>", desc = "Toggle Flote" },
    ["<leader>bn"] = { "<cmd>tabnew<cr>", desc = "New tab" },
    ["<leader>bD"] = {
      function()
        require("astronvim.utils.status").heirline.buffer_picker(
          function(bufnr) require("astronvim.utils.buffer").close(bufnr) end
        )
      end,
      desc = "Pick to close",
    },
    -- tables with the `name` key will be registered with which-key if it's installed
    -- this is useful for naming menus
    ["<leader>b"] = { name = "Buffers" },
    ["<leader>m"] = { name = "Modeline" },
    ["<leader>ml"] = { "<cmd>lua require('user.utils').append_modeline()<cr>", desc = "Append Modeline" },
    ["<left>"] = { "<cmd>tabp<cr>", desc = "Prev Tab" },
    ["<Right>"] = { "<cmd>tabn<cr>", desc = "Next Tab" },
    -- quick save
    -- ["<C-s>"] = { ":w!<cr>", desc = "Save File" },  -- change description but the same command
    L = {
      function() require("astronvim.utils.buffer").nav(vim.v.count > 0 and vim.v.count or 1) end,
      desc = "Next buffer",
    },
    H = {
      function() require("astronvim.utils.buffer").nav(-(vim.v.count > 0 and vim.v.count or 1)) end,
      desc = "Previous buffer",
    },
    ["<leader>="] = { "<cmd>Neotree buffers toggle<cr>", desc = "Neotree Buffers" },
    ["<leader>G"] = { "<cmd>Neotree git_status toggle<cr>", desc = "Neotree Git" },
    ["<c-g>"] = { function() utils.toggle_term_cmd "lazygit" end, desc = "lazygit" },
    ["<leader>tb"] = { function() utils.toggle_term_cmd "btop" end, desc = "btop" },
    ["<leader>tu"] = { function() utils.toggle_term_cmd "gdu-go" end, desc = "Usage - gdu" },
    ["<leader>tf"] = { function() utils.toggle_term_cmd "vifm ." end, desc = "File Manager" },
    ["<leader>tp"] = { function() utils.toggle_term_cmd "ipython" end, desc = "Python" },
    ["<leader>tt"] = { "<cmd>ToggleTerm<cr>", desc = "Terminal" },
    ["<leader>"] = {
      t = {
        name = "ToggleTerm",
      },
      T = {
        name = "Testing",
        a = { "<cmd>wa|TestSuite<cr>", "TestSuite" },
        f = { "<cmd>wa|TestFile<cr>", "TestFile" },
        i = { "<cmd>let test#kotlin#gradletest#options='--info'<cr>", "Info" },
        l = { "<cmd>wa|TestLast<cr>", "TestLast" },
        n = { "<cmd>wa|TestNearest<cr>", "TestNearest" },
        q = { "<cmd>let test#kotlin#gradletest#options=''<cr>", "Quiet" },
        s = { "<cmd>wa|only<cr><cmd>AS<cr>", "Open Test Alternate in H-split" },
        t = { "<cmd>VimuxTogglePane<cr>", "Toggle Vimux Pane" },
        v = { "<cmd>wa|only<cr><cmd>AV<cr>", "Open Test Alternate in V-split" },
        z = { "<cmd>VimuxZoomRunner<cr>", "Zoom Vimux Pane" },
      },
      v = {
        name = "Vimux",
        r = { ":VimuxRunCommand<space>", "Vimux Run Command" },
        t = { "<cmd>VimuxTogglePane<cr>", "Toggle Vimux Pane" },
        z = { "<cmd>VimuxZoomRunner<cr>", "Zoom Vimux Pane" },
      },
      u = {
        z = { ":ZenMode<cr>", "Toggle ZenMode" },
        t = { ":lua require('tint').toggle()<cr>", "Toggle tint" },
      },
    },
  },
  t = {
    -- setting a mapping to false will disable it
    -- ["<esc>"] = false,
    ["<c-g>"] = { "q" },
  },
  v = {
    [">"] = { ">gv" },
    ["<"] = { "<gv" },
  },
}
