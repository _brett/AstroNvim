local M = {}

M.set_global_mark = function(mark, filename, line_nr)
  filename = vim.fn.expand(filename)
  vim.fn.setpos("'" .. mark, { vim.fn.bufnr(filename, 1), line_nr, 1, 0 })
end

M.setup = function()
  local mark = M.set_global_mark
  mark("A", "**/app.py", 1)
  mark("C", "CMakeLists.txt", 1)
  mark("D", ".scratch/database.sql", 4)
  mark("E", "scripts/.local-environment", 1)
  mark("H", ".hammerspoon/init.lua", 4)
  mark("K", "~/.config/kitty/kitty.conf", 26)
  mark("L", ".nvim.lua", 1)
  mark("N", ".notes/flote/notes.md", 4)
  mark("M", "Makefile", 1)
  mark("P", ".projections.json", 4)
  mark("Q", "~/.dotfiles/qmk_firmware/qmk_firmware/keyboards/crkbd/keymaps/brogers/keymap.c", 83)
  mark("R", ".scratch/requests.rest", 4)
  mark("S", ".scratch/scratch.md", 4)
  mark("T", "~/.tmux.conf.local", 350)
  mark("U", "~/.config/nvim/lua/user/plugins/user.lua", 1)
  mark("V", "~/.config/nvim/lua/user/init.lua", 1)
  mark("W", "~/.config/wtf/config.yml", 1)
  mark("Z", "~/.config/zsh/.zshrc", 4)
end

M.add = function(mark, filename, line_nr)
  line_nr = line_nr or 1
  M.set_global_mark(mark, filename, line_nr)
end

return M
