return {
  "nvim-treesitter/nvim-treesitter",
  opts = {
    ensure_installed = { "lua", "http", "json", "sql", "cpp", "c", "objc", "cuda", "proto" },
  },
}
