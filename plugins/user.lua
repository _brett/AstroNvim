return {
  -- You can also add new plugins here as well:
  -- Add plugins, the lazy syntax
  -- "andweeb/presence.nvim",
  -- {
  --   "ray-x/lsp_signature.nvim",
  --   event = "BufRead",
  --   config = function()
  --     require("lsp_signature").setup()
  --   end,
  -- },
  --
  { "tpope/vim-surround", lazy = false },
  { "tpope/vim-repeat", lazy = false },
  { "tpope/vim-dadbod", ft = "sql" },
  { "tpope/vim-projectionist", lazy = false },
  { "tpope/vim-dispatch", lazy = false },
  { "vim-test/vim-test", lazy = false },
  { "ixru/nvim-markdown", ft = "markdown" },
  { "claman/vim-taskpaper", ft = "taskpaper" },
  { "terrortylor/nvim-comment" },
  { "brogers/vim-rest-console", ft = "rest" },
  { "will133/vim-dirdiff", cmd = { "DirDiff" } },
  { "ldelossa/nvim-dap-projects", lazy = false },
  {
    "rest-nvim/rest.nvim",
    dependencies = { "nvim-lua/plenary.nvim" },
    config = function()
      require("rest-nvim").setup {
        -- Open request results in a horizontal split
        result_split_horizontal = false,
        -- Keep the http file buffer above|left when split horizontal|vertical
        result_split_in_place = false,
        -- Skip SSL verification, useful for unknown certificates
        skip_ssl_verification = false,
        -- Encode URL before making request
        encode_url = true,
        -- Highlight request on run
        highlight = {
          enabled = true,
          timeout = 150,
        },
        result = {
          -- toggle showing URL, HTTP info, headers at top the of result window
          show_url = true,
          show_http_info = true,
          show_headers = true,
          -- executables or functions for formatting response body [optional]
          -- set them to false if you want to disable them
          formatters = {
            json = "jq",
            html = function(body) return vim.fn.system({ "tidy", "-i", "-q", "-" }, body) end,
          },
        },
        -- Jump to request line on run
        jump_to_request = true,
        env_file = ".env",
        custom_dynamic_variables = {},
        yank_dry_run = true,
      }
    end,
    ft = "http",
  },
  { "preservim/vimux", config = function() vim.cmd "let test#strategy = 'vimux'" end, lazy = false },
}
