local theme = require("user.highlights.telescope_theme").TelescopeTheme

local bg = "#2d2a45"
local prompt = "#cdcbe0"
local preview_title = "#4b4673"
return theme({}, bg, prompt, preview_title)
