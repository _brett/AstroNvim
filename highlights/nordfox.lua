local theme = require("user.highlights.telescope_theme").TelescopeTheme

local bg = "#39404f"
local prompt = "#5f6f7f"
local preview_title = "#5f6f7f"
return theme({}, bg, prompt, preview_title)
