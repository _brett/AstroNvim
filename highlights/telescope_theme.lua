local M = {}

M.TelescopeTheme = function(hl, bg, prompt, preview_title)
  local C = require "user.highlights.colors"
  local bg_alt = C.bg_1
  local fg = C.fg
  hl.TelescopeBorder = { fg = bg_alt, bg = bg }
  hl.TelescopeNormal = { bg = bg }
  hl.TelescopePreviewBorder = { fg = bg, bg = bg }
  hl.TelescopePreviewNormal = { bg = bg }
  hl.TelescopePreviewTitle = { fg = bg, bg = preview_title }
  hl.TelescopePromptBorder = { fg = bg_alt, bg = bg_alt }
  hl.TelescopePromptNormal = { fg = fg, bg = bg_alt }
  hl.TelescopePromptPrefix = { fg = prompt, bg = bg_alt }
  hl.TelescopePromptTitle = { fg = bg, bg = prompt }
  hl.TelescopeResultsBorder = { fg = bg, bg = bg }
  hl.TelescopeResultsNormal = { bg = bg }
  hl.TelescopeResultsTitle = { fg = bg, bg = bg }

  return hl
end

return M
