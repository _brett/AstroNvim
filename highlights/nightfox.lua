local theme = require("user.highlights.telescope_theme").TelescopeTheme

local bg = "#131a24"
local prompt = "#3c5372"
local preview_title = "#393b44"
return theme({}, bg, prompt, preview_title)
