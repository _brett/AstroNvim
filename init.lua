local vim = vim

return {
  -- Configure AstroNvim updates
  updater = {
    remote = "origin", -- remote to use
    channel = "stable", -- "stable" or "nightly"
    version = "latest", -- "latest", tag name, or regex search like "v1.*" to only do updates before v2 (STABLE ONLY)
    branch = "nightly", -- branch name (NIGHTLY ONLY)
    commit = nil, -- commit hash (NIGHTLY ONLY)
    pin_plugins = nil, -- nil, true, false (nil will pin plugins on stable only)
    skip_prompts = false, -- skip prompts about breaking changes
    show_changelog = true, -- show the changelog after performing an update
    auto_quit = true, -- automatically quit the current session after a successful update
    remotes = { -- easily add new remotes to track
      --   ["remote_name"] = "https://remote_url.come/repo.git", -- full remote url
      --   ["remote2"] = "github_user/repo", -- GitHub user/repo shortcut,
      --   ["remote3"] = "github_user", -- GitHub user assume AstroNvim fork
    },
  },
  -- Set colorscheme to use
  -- colorscheme = "catppuccin-macchiato",
  -- colorscheme = "carbonfox",
  colorscheme = "tokyonight-storm",
  -- Diagnostics configuration (for vim.diagnostics.config({...})) when diagnostics are on
  diagnostics = {
    virtual_text = true,
    underline = true,
  },
  -- Configure require("lazy").setup() options
  lazy = {
    defaults = { lazy = true },
    performance = {
      rtp = {
        -- customize default disabled vim plugins
        disabled_plugins = { "tohtml", "gzip", "matchit", "zipPlugin", "netrwPlugin", "tarPlugin" },
      },
    },
  },
  -- This function is run last and is a good place to configuring
  -- augroups/autocommands and custom filetypes also this just pure lua so
  -- anything that doesn't fit in the normal config locations above can go here
  polish = function()
    -- Set up custom filetypes
    -- vim.filetype.add {
    --   extension = {
    --     foo = "fooscript",
    --   },
    --   filename = {
    --     ["Foofile"] = "fooscript",
    --   },
    --   pattern = {
    --     ["~/%.config/foo/.*"] = "fooscript",
    --   },
    -- }

    vim.g["loaded_perl_provider"] = 0
    -- {{{4 Autosave
    vim.api.nvim_create_autocmd({ "FocusLost" }, {
      pattern = { "*" },
      callback = function() vim.cmd "silent! wa" end,
    })
    -- 4}}}
    -- {{{4 defaultFiles
    vim.api.nvim_create_augroup("defaultFiles", { clear = true })
    vim.api.nvim_create_autocmd("BufNewFile", {
      desc = "REST Template",
      group = "defaultFiles",
      pattern = ".scratch/requests.http",
      command = "0r $HOME/.config/nvim-astronvim/lua/user/templates/scratch.http",
    })
    vim.api.nvim_create_autocmd("BufNewFile", {
      desc = "REST Template",
      group = "defaultFiles",
      pattern = ".scratch/requests.rest",
      command = "0r $HOME/.config/nvim-astronvim/lua/user/templates/scratch.rest",
    })
    vim.api.nvim_create_autocmd("BufNewFile", {
      desc = "Local nvimrc Template",
      group = "defaultFiles",
      pattern = ".nvim.lua",
      command = "0r $HOME/.config/nvim-astronvim/lua/user/templates/nvimrc.local",
    })
    vim.api.nvim_create_autocmd("BufNewFile", {
      desc = "Projectionist Template",
      group = "defaultFiles",
      pattern = ".projections.json",
      command = "0r $HOME/.config/nvim-astronvim/lua/user/templates/projections.json",
    })
    vim.api.nvim_create_autocmd("BufNewFile", {
      desc = "gitignore",
      group = "defaultFiles",
      pattern = ".gitignore",
      command = "0r $HOME/.config/nvim-astronvim/lua/user/templates/.gitignore",
    })
    vim.api.nvim_create_autocmd("BufNewFile", {
      desc = "Dir Env Template",
      group = "defaultFiles",
      pattern = ".envrc",
      command = "0r $HOME/.config/nvim-astronvim/lua/user/templates/envrc",
    })
    vim.api.nvim_create_autocmd("BufNewFile", {
      desc = "SQL Template",
      group = "defaultFiles",
      pattern = ".scratch/database.sql",
      command = "0r $HOME/.config/nvim-astronvim/lua/user/templates/scratch.sql",
    })
    vim.api.nvim_create_autocmd("BufNewFile", {
      desc = "reloadignore Template",
      group = "defaultFiles",
      pattern = ".reloadignore",
      command = "0r $HOME/.config/nvim-astronvim/lua/user/templates/reloadignore",
    })
    vim.api.nvim_create_autocmd("BufNewFile", {
      desc = "Makefile Template",
      group = "defaultFiles",
      pattern = "Makefile",
      command = "0r $HOME/.config/nvim-astronvim/lua/user/templates/Makefile",
    })
    vim.api.nvim_create_autocmd("BufNewFile", {
      desc = "Gradle Build Template",
      group = "defaultFiles",
      pattern = "build.gradle.kts",
      command = "0r $HOME/.config/nvim-astronvim/lua/user/templates/build.gradle.kts",
    })
    vim.api.nvim_create_autocmd("BufNewFile", {
      desc = "pyproject.toml",
      group = "defaultFiles",
      pattern = "pyproject.toml",
      command = "0r $HOME/.config/nvim-astronvim/lua/user/templates/pyproject.toml",
    })
    vim.api.nvim_create_autocmd("BufNewFile", {
      desc = "CMakeLists.txt",
      group = "defaultFiles",
      pattern = "CMakeLists.txt",
      command = "0r $HOME/.config/nvim-astronvim/lua/user/templates/CMakeLists.txt",
    })
    vim.api.nvim_create_autocmd("BufNewFile", {
      desc = "main.cpp",
      group = "defaultFiles",
      pattern = "main.cpp",
      command = "0r $HOME/.config/nvim-astronvim/lua/user/templates/main.cpp",
    })
    vim.api.nvim_create_autocmd("BufNewFile", {
      desc = ".nvim-dap.lua",
      group = "defaultFiles",
      pattern = ".nvim-dap.lua",
      command = "0r $HOME/.config/nvim-astronvim/lua/user/templates/nvim-dap.lua",
    })
    -- 4}}}
    -- python {{{4
    vim.api.nvim_create_augroup("PythonDev", { clear = true })
    vim.api.nvim_create_autocmd("BufRead", {
      desc = "Set Python filetype",
      group = "PythonDev",
      pattern = "*.py",
      callback = require("user.utils").DevelopmentSetup,
    })
    -- 4}}}
    -- duckConsole {{{4
    vim.api.nvim_create_augroup("duckConsole", { clear = true })
    vim.api.nvim_create_autocmd("BufRead", {
      desc = "Set DUCK filetype",
      group = "duckConsole",
      pattern = "*.duck",
      command = "set filetype=duck syntax=sql",
    })
    vim.api.nvim_create_autocmd("BufRead", {
      desc = "Set DUCK filetype",
      group = "duckConsole",
      pattern = "*.duck",
      callback = function()
        local bufnr = vim.api.nvim_get_current_buf()
        vim.api.nvim_buf_set_keymap(bufnr, "n", "<cr>", "vip:ExecCodeBlock<cr>", { noremap = true, silent = true })
        vim.api.nvim_buf_set_keymap(bufnr, "v", "<cr>", ":ExecCodeBlock<cr>", { noremap = true, silent = true })
        vim.api.nvim_buf_set_keymap(bufnr, "n", "<f6>", ":ClearResults<cr>", { noremap = true, silent = true })
        vim.opt_local.foldlevel = 3
        vim.api.nvim_buf_set_option(0, "commentstring", "-- %s")
      end,
    })
    -- 4}}}
    -- restConsole {{{4
    vim.api.nvim_create_augroup("restConsole", { clear = true })
    vim.api.nvim_create_autocmd("FileType", {
      desc = "REST Console",
      group = "restConsole",
      pattern = "http",
      callback = function()
        local bufnr = vim.api.nvim_get_current_buf()
        vim.api.nvim_buf_set_keymap(bufnr, "n", "<cr>", "<Plug>RestNvim", { noremap = true, silent = true })
        vim.opt_local.foldlevel = 3
      end,
    })
    -- 4}}}
    -- vim-rest-console {{{4
    vim.api.nvim_create_augroup("vimRestConsole", { clear = true })
    vim.api.nvim_create_autocmd("FileType", {
      desc = "Vim-REST-Console",
      group = "vimRestConsole",
      pattern = "rest",
      callback = function()
        local bufnr = vim.api.nvim_get_current_buf()
        vim.g["vrc_output_buffer_name"] = "__REST_response__.json"
        vim.api.nvim_buf_del_keymap(bufnr, "n", "<c-j>")
        vim.api.nvim_buf_set_keymap(
          bufnr,
          "n",
          "<cr>",
          "$:w<cr>:execute '!./jwt \"'.getline('.').'\"'<cr>:e<cr>^:call VrcQuery()<cr>\"<cr>",
          { noremap = true, silent = true }
        )
        vim.opt_local.foldlevel = 2
      end,
    })
    -- autocmd FileType rest nnoremap <buffer> <cr> $:execute '!jwt "'.getline('.').'"'<cr>:e<cr>$:call VrcQuery()<cr>
    -- 4}}}
    -- dbConsole {{{4
    vim.api.nvim_create_augroup("dbConsole", { clear = true })
    vim.api.nvim_create_autocmd("BufNewFile", {
      desc = "Set SQL filetype",
      group = "dbConsole",
      pattern = "*.sql",
      command = "set filetype=sql",
    })
    vim.api.nvim_create_autocmd("BufRead", {
      desc = "Set SQL filetype",
      group = "dbConsole",
      pattern = "*.sql",
      command = "set filetype=sql",
    })
    vim.api.nvim_create_autocmd("FileType", {
      desc = "DB Console",
      group = "dbConsole",
      pattern = "sql",
      callback = function()
        local bufnr = vim.api.nvim_get_current_buf()
        vim.api.nvim_buf_set_keymap(bufnr, "n", "<cr>", "V:DB<cr>", { noremap = true, silent = true })
        vim.api.nvim_buf_set_keymap(bufnr, "v", "<cr>", ":DB<cr>", { noremap = true, silent = true })
        vim.api.nvim_buf_set_keymap(bufnr, "n", "<F3>", ":ExecAllBlocks<cr>", { noremap = true, silent = true })
        vim.api.nvim_buf_set_keymap(bufnr, "n", "<F6>", ":ClearResults<CR>", { noremap = true, silent = true })
        vim.api.nvim_buf_set_keymap(bufnr, "n", "<F2>", "vip:ExecCodeBlock<cr>", { noremap = true, silent = true })
        vim.api.nvim_buf_set_keymap(bufnr, "v", "<F2>", ":ExecCodeBlock<cr>", { noremap = true, silent = true })
        vim.opt_local.foldlevel = 3
      end,
    })
    -- 4}}}
    local function reload_flexipatch(project_name, exe)
      if not exe then exe = project_name end
      return function()
        local cwd = vim.fn.getcwd()
        if string.find(cwd, project_name, 1, true) then
          vim.api.nvim_command(
            "silent !notify-send 'Reloading' '"
              .. project_name
              .. "'; cd "
              .. cwd
              .. "; rm -f config.h patches.h; sudo make clean install && { kill -HUP $(pidof "
              .. exe
              .. ")}"
          )
        end
      end
    end
    -- dwm {{{4
    vim.api.nvim_create_augroup("dwmreload", { clear = true })
    vim.api.nvim_create_autocmd("BufWritePost", {
      desc = "dwm Reload",
      group = "dwmreload",
      pattern = "config.def.h",
      callback = reload_flexipatch("dwm-flexipatch", "dwm"),
    })
    -- 4}}}
    -- st {{{4
    vim.api.nvim_create_augroup("streload", { clear = true })
    vim.api.nvim_create_autocmd("BufWritePost", {
      desc = "st Reload",
      group = "streload",
      pattern = "config.def.h",
      callback = reload_flexipatch("st-flexipatch", "st"),
    })
    -- 4}}}
    -- scratch {{{4
    vim.api.nvim_create_augroup("scratch", { clear = true })
    vim.api.nvim_create_autocmd("FileType", {
      desc = "Scratch files",
      group = "scratch",
      pattern = ".scratch.*",
      callback = function()
        local bufnr = vim.api.nvim_get_current_buf()
        vim.api.nvim_buf_set_keymap(bufnr, "n", "<cr>", "vip:ExecCodeBlock<cr>", { silent = true })
        vim.api.nvim_buf_set_keymap(bufnr, "v", "<cr>", ":ExecCodeBlock<cr>", { silent = true })
        vim.api.nvim_buf_set_keymap(bufnr, "n", "<F3>", ":ExecAllBlocks<cr>", { noremap = true, silent = true })
        vim.api.nvim_buf_set_keymap(bufnr, "n", "<F6>", ":ClearResults<CR>", { noremap = true, silent = true })
        vim.api.nvim_buf_set_keymap(bufnr, "n", "<F2>", "vip:ExecCodeBlock<cr>", { noremap = true, silent = true })
        vim.api.nvim_buf_set_keymap(bufnr, "v", "<F2>", ":ExecCodeBlock<cr>", { noremap = true, silent = true })
      end,
    })
    -- 4}}}
    -- 4}}}
    require("user.code-blocks").setup {
      execs = {
        pio = { exec = "pio" },
        wibble = { exec = "echo" },
        dj = { exec = "duckdb", opts = "-json -c", prefix = "dj>" },
        dt = { exec = "duckdb", opts = "ticker.db -box -c", prefix = "dt>" },
        sql = { exec = "duckdb", opts = "-json -c", prefix = "sql>" },
        df = { exec = "duckdb", opts = "futures.db -box -c", prefix = "df>" },
      },
    }
    require("user.global-marks").setup()
    require("nvim-dap-projects").search_project_config()
  end,
}
