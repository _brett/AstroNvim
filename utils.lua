local M = {}
local vim = vim

M.append_modeline = function()
  local bufnr = vim.api.nvim_get_current_buf()
  local modeline = " vim: set ft="
    .. vim.bo.filetype
    .. " ts="
    .. vim.opt.tabstop._value
    .. " sts="
    .. vim.opt.softtabstop._value
    .. " sw="
    .. vim.opt.shiftwidth._value
    .. " tw="
    .. vim.opt.textwidth._value
    .. " foldlevel="
    .. vim.opt.foldlevel._value
    .. " :"
  modeline = vim.opt.commentstring._value:format(modeline)
  vim.api.nvim_buf_set_lines(bufnr, -1, -1, true, { modeline })
end

M.DevelopmentSetup = function()
  local bufnr = vim.api.nvim_get_current_buf()
  vim.api.nvim_buf_set_keymap(
    bufnr,
    "n",
    "<cr>",
    "<cmd>lua require('user.utils').RunTest()<cr>",
    { noremap = true, silent = true }
  )
  vim.opt_local.foldlevel = 5
  vim.api.nvim_set_keymap("n", "<F3>", ":ExecAllBlocks<cr>", { noremap = true, silent = true })
  vim.api.nvim_set_keymap("n", "<F6>", ":ClearResults<CR>", { noremap = true, silent = true })
  vim.api.nvim_set_keymap("n", "<F2>", "vip:ExecCodeBlock<cr>", { noremap = true, silent = true })
  vim.api.nvim_set_keymap("v", "<F2>", ":ExecCodeBlock<cr>", { noremap = true, silent = true })
end

M.RunTest = function()
  vim.api.nvim_command "wall"
  local bufnr = vim.api.nvim_get_current_buf()
  local buf_name = vim.api.nvim_buf_get_name(bufnr)
  local function in_test_file(filename)
    if filename:find "test_.+%.py" or filename:find "Test.+%.kt" or filename:find "Test.+%.java" then return true end
    return false
  end
  if in_test_file(buf_name) then
    vim.api.nvim_command "TestNearest"
  else
    vim.api.nvim_command "TestLast"
  end
end

M.GetVisual = function()
  local s_start = vim.fn.getpos "'<"
  local s_end = vim.fn.getpos "'>"
  local n_lines = math.abs(s_end[2] - s_start[2]) + 1
  local lines = vim.api.nvim_buf_get_lines(0, s_start[2] - 1, s_end[2], false)
  lines[1] = string.sub(lines[1], s_start[3], -1)
  if n_lines == 1 then
    lines[n_lines] = string.sub(lines[n_lines], 1, s_end[3] - s_start[3] + 1)
  else
    lines[n_lines] = string.sub(lines[n_lines], 1, s_end[3])
  end
  return lines
end

return M
