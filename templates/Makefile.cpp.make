SHELL := bash
.ONESHELL:
.SHELLFLAGS := -eu -o pipefail -c
.DELETE_ON_ERROR:
MAKEFLAGS += --warn-undefined-variables
MAKEFLAGS += --no-builtin-rules
MAKEFLAGS += --no-print-directory

.DEFAULT_GOAL = help

APP := main
BUILD := build
SOURCE := .
MARKER := .initialized

SOURCES := $(wildcard **/*.cpp *.cpp **/*.h *.h)
CONFIGS := $(wildcard CMakeLists.txt **/CMakeLists.txt)

$(BUILD)/$(MARKER):
$(BUILD)/$(MARKER): $(SOURCES) $(CONFIGS) 
	@cmake -B$(BUILD) -S .
	@touch $(BUILD)/$(MARKER)

config: ## Build project
config: $(BUILD)/$(MARKER)

.PHONY: clean
clean: ## Remove build directory
clean:
	@$(RM) -rf $(BUILD)


$(BUILD)/$(APP): $(BUILD)/$(MARKER)
	@VERBOSE=1 MAKESILENT=-s COLOR=ON $(MAKE) -C $(BUILD) $(APP)

.PHONY: run
run: ## Run example
run: $(BUILD)/$(APP)
run:
	@./$(BUILD)/$(APP)

.PHONY: help
help: ## Show help message
	@awk 'BEGIN {FS = ":.*##"; printf "\nUsage:\n  make \033[36m\033[0m\n"} /^[$$()% 0-9a-zA-Z_-]+:.*?##/ { printf "  \033[36m%-15s\033[0m %s\n", $$1, $$2 } /^##@/ { printf "\n\033[1m%s\033[0m\n", substr($$0, 5) } ' $(MAKEFILE_LIST)

#  vim: set ft=make ts=8 sts=0 sw=0 tw=0 foldlevel=0 foldmethod=marker noet :
