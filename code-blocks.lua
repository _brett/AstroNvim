local M = {}

local function result_begin() return vim.g.code_blocks_result_begin or "---------Result----------" end
local function result_end() return vim.g.code_blocks_result_end or "---------END-------------" end
local get_visual = require("user.utils").GetVisual
local get_comment_wrapper = require("nvim_comment").get_comment_wrapper
local next = next

local function startswith(text, prefix)
  if not prefix then
    return false
  else
    return text:sub(1, #prefix) == prefix
  end
end

local function comment_line(line)
  local left_comment_marker, right_comment_marker = get_comment_wrapper()
  local cl = require("nvim_comment").comment_line
  return cl(line, "", left_comment_marker, right_comment_marker, true, true)
end

local function uncomment_line(line)
  local left_comment_marker, right_comment_marker = get_comment_wrapper()
  local ucl = require("nvim_comment").uncomment_line
  if left_comment_marker then return ucl(line, left_comment_marker, right_comment_marker, false) end
  return line
end

local function uncomment_lines(lines)
  local left_comment_marker, _ = get_comment_wrapper()
  for i, line in pairs(lines) do
    if startswith(line:gsub("^%s*", ""), left_comment_marker) then lines[i] = uncomment_line(line) end
  end
  return lines
end

local function clear_results()
  local bufnr = vim.api.nvim_get_current_buf()
  vim.api.nvim_win_set_cursor(0, { 1, 1 })
  while vim.fn.search(result_begin(), "W") ~= 0 do
    local s = vim.api.nvim_win_get_cursor(0)[1] - 2
    vim.fn.search(result_end(), "W")
    local e = vim.api.nvim_win_get_cursor(0)[1]
    vim.api.nvim_buf_set_lines(bufnr, s, e, true, {})
    local pos = e - s
    if pos < 1 then pos = 1 end
    if pos > vim.api.nvim_buf_line_count(bufnr) then pos = vim.api.nvim_buf_line_count(bufnr) end

    vim.api.nvim_win_set_cursor(0, { pos, 1 })
  end
  vim.api.nvim_win_set_cursor(0, { 1, 1 })
end

local known_execs = {
  default = { exec = vim.g.code_blocks_default or "bash", opts = "-c", prefix = ">" },
  bash = { exec = vim.g.code_blocks_shell or "bash", opts = "-c", prefix = "bash>" },
  duck = { exec = vim.g.code_blocks_duck or "duckdb", opts = "-box -c", prefix = "d>" },
  d = { exec = vim.g.code_blocks_duck or "duckdb", opts = "-box -c", prefix = "d>" },
  df = { exec = vim.g.code_blocks_duck or "duckdb", opts = "futures.db -box -c", prefix = "df>" },
  jq = { exec = vim.g.code_blocks_jq or "jq", opts = "-c", prefix = "jq>" },
  lua = { exec = vim.g.code_blocks_lua or "lua", opts = "-e", prefix = "lua>" },
  py = { exec = vim.g.code_blocks_python or "python", opts = "-c", prefix = "py>" },
  z = { exec = vim.g.code_blocks_zsh or "zsh", opts = "-c", prefix = "z>" },
  zsh = { exec = vim.g.code_blocks_zsh or "zsh", opts = "-c", prefix = "zsh>" },
}

local function execs(prompt)
  local exec = known_execs[prompt]
  if not exec then
    local known = ""
    for k, _ in pairs(known_execs) do
      known = known .. " " .. k
    end
    vim.notify(
      "Unknown Exec: " .. prompt .. " Known execs: " .. known .. "\n Attempting to use " .. prompt .. " as exec",
      vim.log.levels.ERROR,
      { title = "Code Blocks" }
    )
    exec = { exec = prompt, opts = "" }
  end
  return exec
end

local function extract_commands(v_selection)
  local c = 1
  local lines = {}
  local last_line
  local commands = {}
  local first_line = table.remove(v_selection, 1)
  if not first_line then first_line = "" end
  local prompt = first_line:match "^(%a*)>.*"
  if not prompt then prompt = "" end
  if first_line:match "^%a+>%s*(.*)" ~= "" then
    first_line = first_line:gsub("^%a*>%s*", "")
    table.insert(v_selection, 1, first_line)
  end
  if prompt == "" then prompt = "default" end
  for _, line in pairs(v_selection) do
    if startswith(line:gsub("^%s*", ""), "<end") then
      c = c + 1
      lines = {}
    else
      table.insert(lines, line)
      last_line = line
      commands[c] = { lines = lines, last_line = last_line, exec = execs(prompt) }
    end
  end
  return commands
end

-- local function exec_shell(command)
--   local timeout = vim.g.code_blocks_timeout or 2
--   local tmpfname = os.tmpname()
--   local result = nil
--   (sleep 100) & (echo $! > /tmp/wibble ) & ( sleep 10; cat /tmp/wibble | xargs kill -9; echo killed the sleeper )
--   local f = io.popen(
--     string.format("(%s > %s; echo shell_done) & " .. "(sleep %s; echo shell_timeout)", command, tmpfname, timeout)
--   )
--   if f then result = f:read "*line" end
--   if result == "shell_done" then return io.lines(tmpfname) end
-- end

local function exec_fn_systemlist(command) return vim.fn.systemlist(command) end

local function exec_command(cmd, linenr)
  local bufnr = vim.api.nvim_get_current_buf()
  local statement = table.concat(cmd.lines, "\n"):gsub('"', '\\"')
  local exec = cmd.exec
  local command = ([[%s %s "%s" 2>&1]]):format(exec.exec, exec.opts, statement)

  local result = {}

  if result_begin() ~= "" then table.insert(result, result_begin()) end
  -- local lines = exec_shell(command)
  local lines = exec_fn_systemlist(command)
  if lines then
    -- for line in lines do
    for _, line in ipairs(lines) do
      if vim.g.code_blocks_prefix == 1 then line = exec.prefix .. " " .. line end
      table.insert(result, line)
    end
    if result_end() ~= "" then table.insert(result, result_end()) end

    if vim.g.code_blocks_comment_result ~= 0 then
      for i, line in pairs(result) do
        result[i] = comment_line(line)
      end
    end

    table.insert(result, 1, "")

    vim.api.nvim_buf_set_lines(bufnr, linenr, linenr, true, result)
  else
    vim.notify("Command Failed", vim.log.levels.ERROR, { title = "Code Blocks" })
  end
end

local function exec_code()
  local pos = vim.fn.getpos "'>"
  local insert_linenr = pos[2]
  if vim.g.insert_after_fence == 1 then insert_linenr = vim.fn.search("```", "W") end
  local lines = uncomment_lines(get_visual())
  local prompt = lines[1]:gsub(">.*", "> ")
  if prompt == "" then prompt = "default" end
  lines[1] = lines[1]:gsub("^%a*>%s*", "")
  table.insert(lines, 1, prompt)
  if lines[#lines] ~= "<end" then table.insert(lines, "<end") end
  local commands = extract_commands(lines)

  for _, cmd in pairs(commands) do
    exec_command(cmd, insert_linenr)
  end

  vim.api.nvim_win_set_cursor(0, { pos[2], pos[3] })
end

local function is_prompt(line)
  if not line or line == "" then return false end
  return uncomment_line(line):match "^%a*>"
end

local function next_prompt(linenr)
  local bufnr = vim.api.nvim_get_current_buf()
  vim.api.nvim_win_set_cursor(0, { linenr, 0 })
  while linenr ~= 0 do
    local prompt = vim.api.nvim_buf_get_lines(bufnr, linenr - 1, linenr, true)
    if next(prompt) and is_prompt(prompt[1]) then
      return linenr
    else
      linenr = vim.fn.search("^.*\\w*>", "W")
    end
  end
  return 0
end

local function exec_blocks()
  local bufnr = vim.api.nvim_get_current_buf()
  local start_linenr = next_prompt(1)

  while start_linenr ~= 0 do
    local end_linenr = vim.fn.search "<end"
    local lines = uncomment_lines(vim.api.nvim_buf_get_lines(bufnr, start_linenr - 1, end_linenr, true))
    local commands = extract_commands(lines)

    for _, cmd in pairs(commands) do
      exec_command(cmd, end_linenr)
    end
    start_linenr = next_prompt(end_linenr)
  end

  vim.api.nvim_win_set_cursor(0, { 1, 0 })
end

M.setup = function(opts)
  if opts and opts["execs"] then
    for k, v in pairs(opts.execs) do
      if v["exec"] then
        if not v["opts"] then v["opts"] = "" end
        if not v["prefix"] then v["prefix"] = v.exec .. ">" end
        known_execs[k] = v
      else
        vim.notify(
          "Unknown options: " .. k .. " = { " .. vim.inspect(opts[k]) .. " }",
          vim.log.levels.ERROR,
          { title = "Code Blocks" }
        )
      end
    end
  end
  vim.api.nvim_create_user_command("ExecAllBlocks", exec_blocks, { bang = true, desc = "Execute all code blocks" })

  vim.api.nvim_create_user_command("ClearResults", clear_results, { bang = true, desc = "Remove Results Blocks" })

  vim.api.nvim_create_user_command(
    "ExecCodeBlock",
    exec_code,
    { bang = true, desc = "Execute selected code block", range = true }
  )
end
return M
